class Tempf {
  constructor ( dato = 0 ) {
    this._dato = dato;
  }

  set dato ( valor ) {
    this._dato = valor;
  }

  get tempf() {
    return ((((this._dato) * 9/5)) + 32)
  }
}

module.exports = Tempf