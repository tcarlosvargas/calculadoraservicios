var express = require('express');
var Sen = require('./trigonometrico.js')
var Tempc = require('./tempc.js')
var Tempf = require('./tempf.js')
var Usd_cop = require('./usd_cop.js')
var Cop_usd = require('./cop_usd.js')
var app = express();

app.get('/seno/:number', function(req, res){
	var number = req.params.number
	let seno = new Sen(number);

	let result_body = {number: number, function: 'seno', result: seno.seno}

	res.json(result_body)
})

app.get('/tempc/:number', function(req, res){
	var number = req.params.number
	let tempc = new Tempc(number);

	let result_body = {number: number, function: 'tempc', result: tempc.tempc}

	res.json(result_body)
})


app.get('/tempf/:number', function(req, res){
	var number = req.params.number
	let tempf = new Tempf(number);

	let result_body = {number: number, function: 'tempf', result: tempf.tempf}

	res.json(result_body)
})

app.get('/cop_usd/:number', function(req, res){
	var number = req.params.number
	let cop_usd = new Cop_usd(number);

	let result_body = {number: number, function: 'cop_usd', result: cop_usd.cop_usd}

	res.json(result_body)
})

app.get('/usd_cop/:number', function(req, res){
	var number = req.params.number
	let usd_cop = new Usd_cop(number);

	let result_body = {number: number, function: 'usd_cop', result: usd_cop.usd_cop}

	res.json(result_body)
})

app.listen(8080, function(){
	console.log('Server running on port 8080')
})