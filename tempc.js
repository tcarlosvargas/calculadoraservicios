class Tempc {
  constructor ( dato = 0 ) {
    this._dato = dato;
  }

  set dato ( valor ) {
    this._dato = valor;
  }

  get tempc() {
    return ((((this._dato) - 32) * (5/9)))
  }
}

module.exports = Tempc